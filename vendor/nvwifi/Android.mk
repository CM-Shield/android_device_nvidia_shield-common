# Copyright (C) 2022 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)
SHIELD_NVWIFI_PATH := ../../../../../vendor/nvidia/shield/nvwifi

include $(CLEAR_VARS)
LOCAL_MODULE               := wpa_supplicant-nvwifi
LOCAL_MODULE_STEM          := wpa_supplicant
LOCAL_SRC_FILES_32         := $(SHIELD_NVWIFI_PATH)/bin32/hw/wpa_supplicant
LOCAL_SRC_FILES_64         := $(SHIELD_NVWIFI_PATH)/bin64/hw/wpa_supplicant
LOCAL_MULTILIB             := first
LOCAL_INIT_RC              := etc/init/wpa_supplicant-nvwifi.rc
LOCAL_VINTF_FRAGMENTS      := vendor.nvidia.hardware.nvwifi@2.0-service.xml
LOCAL_MODULE_CLASS         := EXECUTABLES
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_OWNER         := nvidia
LOCAL_VENDOR_MODULE        := true
include $(BUILD_NVIDIA_PREBUILT)
